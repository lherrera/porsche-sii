﻿
namespace PorscheSII
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class SignatureMessage
    {

        private SignatureMessageDocument documentField;

        private SignatureMessageStoreInfo storeInfoField;

        private SignatureMessageAuthorization authorizationField;

        private SignatureMessageStatus statusField;

        private object extensionsField;

        private string typeField;

        private System.DateTime tstampField;

        /// <remarks/>
        public SignatureMessageDocument Document
        {
            get
            {
                return this.documentField;
            }
            set
            {
                this.documentField = value;
            }
        }

        /// <remarks/>
        public SignatureMessageStoreInfo StoreInfo
        {
            get
            {
                return this.storeInfoField;
            }
            set
            {
                this.storeInfoField = value;
            }
        }

        /// <remarks/>
        public SignatureMessageAuthorization Authorization
        {
            get
            {
                return this.authorizationField;
            }
            set
            {
                this.authorizationField = value;
            }
        }

        /// <remarks/>
        public SignatureMessageStatus Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public object Extensions
        {
            get
            {
                return this.extensionsField;
            }
            set
            {
                this.extensionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime tstamp
        {
            get
            {
                return this.tstampField;
            }
            set
            {
                this.tstampField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SignatureMessageDocument
    {

        private string senderField;

        private string receiverField;

        private int groupField;

        private int typeField;

        private uint numberField;

        private System.DateTime dateField;

        private string idField;

        /// <remarks/>
        public string Sender
        {
            get
            {
                return this.senderField;
            }
            set
            {
                this.senderField = value;
            }
        }

        /// <remarks/>
        public string Receiver
        {
            get
            {
                return this.receiverField;
            }
            set
            {
                this.receiverField = value;
            }
        }

        /// <remarks/>
        public int Group
        {
            get
            {
                return this.groupField;
            }
            set
            {
                this.groupField = value;
            }
        }

        /// <remarks/>
        public int Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public uint Number
        {
            get
            {
                return this.numberField;
            }
            set
            {
                this.numberField = value;
            }
        }

        /// <remarks/>
        public System.DateTime Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SignatureMessageStoreInfo
    {

        private System.DateTime timeStampField;

        private System.DateTime updateTStampField;

        private string xmlFileField;

        private string pdfFileField;

        private string urlField;

        /// <remarks/>
        public System.DateTime TimeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        public System.DateTime UpdateTStamp
        {
            get
            {
                return this.updateTStampField;
            }
            set
            {
                this.updateTStampField = value;
            }
        }

        /// <remarks/>
        public string XmlFile
        {
            get
            {
                return this.xmlFileField;
            }
            set
            {
                this.xmlFileField = value;
            }
        }

        /// <remarks/>
        public string PdfFile
        {
            get
            {
                return this.pdfFileField;
            }
            set
            {
                this.pdfFileField = value;
            }
        }

        /// <remarks/>
        public string Url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SignatureMessageAuthorization
    {

        private uint codeField;

        private uint trackIdField;

        private SignatureMessageAuthorizationStatus statusField;

        /// <remarks/>
        public uint Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public uint TrackId
        {
            get
            {
                return this.trackIdField;
            }
            set
            {
                this.trackIdField = value;
            }
        }

        /// <remarks/>
        public SignatureMessageAuthorizationStatus Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SignatureMessageAuthorizationStatus
    {

        private byte codeField;

        private string descriptionField;

        private System.DateTime timeStampField;

        private string commentsField;

        /// <remarks/>
        public byte Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public System.DateTime TimeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SignatureMessageStatus
    {

        private byte codeField;

        private string descriptionField;

        private System.DateTime timeStampField;

        private uint trackIdField;

        private string commentsField;

        /// <remarks/>
        public byte Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public System.DateTime TimeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        public uint TrackId
        {
            get
            {
                return this.trackIdField;
            }
            set
            {
                this.trackIdField = value;
            }
        }

        /// <remarks/>
        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }
    }


}
