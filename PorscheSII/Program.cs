﻿using System;
using BizArk.Core.CmdLine;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using System.IO;

namespace PorscheSII
{
    public class Argumentos
        : CmdLineObject
    {
        [CmdLineArg(Alias = "e", Usage = "Rut", Required = true)]
        [Description("RUT del emisor del documento.")]
        public string Emisor { get; set; }

        [CmdLineArg(Alias = "u", Usage = "Rut", Required = true)]
        [Description("RUT del certificado digital usado.")]
        public string Usuario { get; set; }

        [CmdLineArg(Alias = "l", Usage = "Archivo", Required = true)]
        [Description("Carpeta donde se encuentra el archivo de Licencia Pacioli (licencia.xml).")]
        public string Licencia { get; set; }

        [CmdLineArg(Alias = "r", Usage = "Rut", Required = true)]
        [Description("RUT del receptor del documento.")]
        public string RUT { get; set; }

        [CmdLineArg(Alias = "f", Usage = "Fecha", Required = true)]
        [Description("Fecha del documento.")]
        public DateTime Fecha { get; set; }

        [CmdLineArg(Alias = "n", Usage = "Numero", Required = true)]
        [Description("Numero de folio.")]
        public uint NumeroDeFolio { get; set; }

        [CmdLineArg(Alias = "t", Usage = "Total", Required = true)]
        [Description("Monto total del documento.")]
        public int Total { get; set; }
        [CmdLineArg(Alias = "d", Usage = "TipoDTE", Required = true)]
        [Description("Código de Tipo de Documento SII.")]
        public int TipoDTE { get; set; }

        [CmdLineArg(Usage = "StoreInfoTimeStamp", Required = true)]
        [Description("Valor de nodo StoreInfoTimeStamp")]
        public DateTime StoreInfoTimeStamp { get; set; }

        [CmdLineArg(Usage = "UpdateTStamp", Required = true)]
        [Description("Valor de nodo UpdateTStamp")]
        public DateTime UpdateTStamp { get; set; }

        [CmdLineArg(Usage = "XmlFile", Required = true)]
        [Description("Valor de nodo XmlFile")]
        public string XmlFile { get; set; }

        [CmdLineArg(Usage = "PdfFile", Required = true)]
        [Description("Valor de nodo PdfFile")]
        public string PdfFile { get; set; }

        [CmdLineArg(Usage = "Url", Required = true)]
        [Description("Valor de nodo Url")]
        public string Url { get; set; }

        [CmdLineArg(Usage = "Group", Required = true)]
        [Description("Valor de nodo Group")]
        public int Group { get; set; }

        [CmdLineArg(Usage = "Id", Required = true)]
        [Description("Valor de nodo Id")]
        public string Id { get; set; }

        [CmdLineArg(Usage = "TrackId", Required = true)]
        [Description("Valor de nodo TrackId")]
        public uint TrackId { get; set; }

        [CmdLineArg(Alias = "o", Usage = "Directorio de salida", Required = true)]
        [Description("Directorio donde se guardará archivo de salida")]
        public string Output { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ConsoleApplication.RunProgram<Argumentos>(Run);
        }

        private static string Verificar(Argumentos arg)
        {
            Process p = new Process();

            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = "pacioli_dte_check.exe";
            p.StartInfo.Arguments = String.Format(
                "-l \"{0}\" -r {1} -d {2} -t {3} -f {4:dd-MM-yyyy} -n {5} -u {6}",
                arg.Licencia, arg.RUT, arg.TipoDTE, arg.Total, arg.Fecha, arg.NumeroDeFolio, arg.Usuario
            );

            p.Start();
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            return output;
        }

        private static RESPUESTA LeerRespuesta(string src)
        {
            RESPUESTA result = null;
            var serializer = new XmlSerializer(typeof(RESPUESTA));
            using (var reader = new StringReader(src))
            {
                try
                {
                    result = (RESPUESTA)serializer.Deserialize(reader);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error leyendo respuesta:");
                    Console.WriteLine("------------------------");
                    Console.WriteLine(src);
                    Console.WriteLine("------------------------");
                    throw (e);
                }
            }
            return result;
        }

        private static void Run(Argumentos arg)
        {
            string resp = Verificar(arg);
            var obj = LeerRespuesta(resp);
            var msg = new SignatureMessage()
            {
                type = "MSGSAS",
                tstamp = DateTime.Now,
                Document = new SignatureMessageDocument()
                {
                    Id = arg.Id,
                    Sender = arg.Emisor,
                    Receiver = arg.RUT,
                    Group = arg.Group,
                    Type = arg.TipoDTE,
                    Number = arg.NumeroDeFolio,
                    Date = arg.Fecha
                },
                StoreInfo = new SignatureMessageStoreInfo()
                {
                    TimeStamp = arg.StoreInfoTimeStamp,
                    UpdateTStamp = arg.UpdateTStamp,
                    XmlFile = arg.XmlFile,
                    PdfFile = arg.PdfFile,
                    Url = arg.Url
                },
                Authorization = new SignatureMessageAuthorization()
                {
                    Code = arg.NumeroDeFolio,
                    TrackId = arg.TrackId
                },
            };

            var Status = new SignatureMessageStatus()
                {
                    TimeStamp = DateTime.Now,
                    TrackId = arg.TrackId
                };
            Status.Comments = obj.RESP_HDR.NUM_ATENCION;
            /* Códigos SII:
             * 0: Documento Recibido por el SII. Datos Coinciden con los Registrados. 
             * 1: Documento Recibido por el SII pero Datos NO Coinciden con los registrados. 
             * 3: Documento No Recibido por el SII. 
             * 4: Documento No Autorizado
             * 5: Documento Anulado
             * 6: Empresa no autorizada a emitir DTE
             * 10: Existe nota de débito que modifica texto
             * 11: Existe nota de crédito que modifica texto
             * 12: Existe nota de débito que modifica montos
             * 13: Existe nota de crédito que modifica montos
             * 14: Existe Nota de débito que anula documento
             * 15: Existe nota de crédito que anula documento
             * Otro: error interno
             */           
            if (obj.RESP_HDR.ERR_CODE == 0)
            {
                Status.Code = 4;
                Status.Description = "Aprobado SII";
            }
            else
            {                
                if (obj.RESP_HDR.ERR_CODE == 3)  //no recibido por SII 
                {
                    Status.Code = 5;
                    Status.Description = "No Recibido";
                }
                else
                {
                    Status.Code = 6;
                    Status.Description = "Rechazado SII";
                }
            }

            msg.Authorization.Status = new SignatureMessageAuthorizationStatus()
            {
                Code = Status.Code,
                Comments = Status.Comments,
                Description = Status.Description,
                TimeStamp = Status.TimeStamp
            };
            msg.Status = Status;
            var filename =
                Path.Combine(arg.Output, String.Format("{3:yyy}.{0}.{1}.{2}.txt", arg.RUT, arg.TipoDTE, arg.NumeroDeFolio, arg.Fecha));

            var serializer = new XmlSerializer(msg.GetType());
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            var file = new StreamWriter(filename);
            serializer.Serialize(file, msg, ns);
            file.Close();
        }
    }
}
