﻿
namespace PorscheSII
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sii.cl/XMLSchema")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.sii.cl/XMLSchema", IsNullable = false)]
    public partial class RESPUESTA
    {

        private RESPUESTARESP_HDR rESP_HDRField;

        /// <remarks/>
        public RESPUESTARESP_HDR RESP_HDR
        {
            get
            {
                return this.rESP_HDRField;
            }
            set
            {
                this.rESP_HDRField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sii.cl/XMLSchema")]
    public partial class RESPUESTARESP_HDR
    {

        private string eSTADOField;

        private string gLOSA_ESTADOField;

        private byte eRR_CODEField;

        private string gLOSA_ERRField;

        private string nUM_ATENCIONField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string ESTADO
        {
            get
            {
                return this.eSTADOField;
            }
            set
            {
                this.eSTADOField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string GLOSA_ESTADO
        {
            get
            {
                return this.gLOSA_ESTADOField;
            }
            set
            {
                this.gLOSA_ESTADOField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public byte ERR_CODE
        {
            get
            {
                return this.eRR_CODEField;
            }
            set
            {
                this.eRR_CODEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string GLOSA_ERR
        {
            get
            {
                return this.gLOSA_ERRField;
            }
            set
            {
                this.gLOSA_ERRField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string NUM_ATENCION
        {
            get
            {
                return this.nUM_ATENCIONField;
            }
            set
            {
                this.nUM_ATENCIONField = value;
            }
        }
    }


}
